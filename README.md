# User Default Page

User Default Page module provide you the facility to customize the
destination that a user is redirected to after logging in or logged out.

This module provides the below facilities:

- It provides a configuration setting in UI for admin user.
- You can Add, Edit or Delete a User default page.
- You can give permission the User default page as per the Roles or
  User or Both.
- You can set the url path for login and logout action along with
  success messages.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/user_default_page).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/user_default_page).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Go to: Configuration >> People >> User Default Page
   (`/admin/config/people/user_default_page`).
   The table will look empty initially as you have not created a User
   Default Page yet.
2. Create a user default page by clicking on `"Add User default page"`
   button or from `/admin/config/people/user_default_page/add`.
3. Select a user role(from: User Roles field), for which you are going
   to use the User Default page or an individual user (from: Select User field)
   or both.
4. Fill the `Redirect to URL` field and `Message` field for both Login
   and Logout fieldset.
5. Click on the save button to save the configuration.


## Maintainers

- Krishna Kanth - [krknth](https://www.drupal.org/u/krknth)
- Mahaveer Singh Panwar - [mahaveer003](https://www.drupal.org/u/mahaveer003)
- Neeraj Kumar - [neerajskydiver](https://www.drupal.org/u/neerajskydiver)
- Jeff Markel - [jmarkel](https://www.drupal.org/u/jmarkel)
- Vladimir Roudakov - [VladimirAus](https://www.drupal.org/u/vladimiraus)
- A Ajay Kumar Reddy - [ajay_reddy](https://www.drupal.org/u/ajay_reddy)
- Nishant kumar - [nishantkumar155](https://www.drupal.org/u/nishantkumar155)
